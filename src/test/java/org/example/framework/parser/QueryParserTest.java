package org.example.framework.parser;

import org.example.framework.http.Request;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class QueryParserTest {
    @Test
    void shouldParse() {
        final Request requestIn = new Request();
        requestIn.setQuery("text=java&type=ebook&type=course");
        final Map<String, List<String>> queryParamsActual = QueryParser.parse(requestIn).getQueryParams();
        final Request requestOut = new Request();
        requestOut.setQuery("text=java&type=ebook&type=course");
        requestOut.getQueryParams().putAll(queryParamsActual);
        Map<String, List<String>> queryParams = new LinkedHashMap<>();
        queryParams.put("text", Arrays.asList("java"));
        queryParams.put("type", Arrays.asList("ebook", "course"));
        assertEquals(queryParams, requestOut.getQueryParams());

        assertEquals(queryParams, requestOut.getAllQueryParams());
        assertEquals(Arrays.asList("ebook", "course"), requestOut.getQueryParams("type"));
        assertEquals(requestOut, QueryParser.parse(requestIn));
        assertEquals(Optional.of("ebook"), requestOut.getQueryParam("type"));
        assertEquals(Optional.empty(), requestOut.getQueryParam("qwer"));

    }
}
