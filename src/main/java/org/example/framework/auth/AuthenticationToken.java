package org.example.framework.auth;


public interface AuthenticationToken {
  Object getLogin();
  Object getCredentials();
}
