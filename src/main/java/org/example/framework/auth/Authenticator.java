package org.example.framework.auth;

public interface Authenticator {
  boolean authenticate(AuthenticationToken request);
}
