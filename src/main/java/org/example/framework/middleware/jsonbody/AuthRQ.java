package org.example.framework.middleware.jsonbody;

import lombok.Data;

@Data
public class AuthRQ {
  private String username;
  private String password;
}
