package org.example.framework.middleware.jsonbody;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.Authenticator;
import org.example.framework.auth.LoginPasswordAuthenticationToken;
import org.example.framework.auth.SecurityContext;
import org.example.framework.auth.principal.LoginPrincipal;
import org.example.framework.exception.AuthenticationException;
import org.example.framework.http.Request;
import org.example.framework.middleware.Middleware;

import java.net.Socket;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
public class JSONBodyAuthNMiddleware implements Middleware {
  private final Gson gson;
  private final Authenticator authenticator;

  @Override
  public void handle(final Socket socket, final Request request) {
    if (SecurityContext.getPrincipal() != null) {
      return;
    }
    try {

      final AuthRQ authRQ = gson.fromJson(new String(request.getBody(), StandardCharsets.UTF_8), AuthRQ.class);

      final LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(authRQ.getUsername(), authRQ.getPassword());
      if (!authenticator.authenticate(authRequest)) {
        throw new AuthenticationException("can't authenticate");
      }
      SecurityContext.setPrincipal(new LoginPrincipal(authRQ.getUsername()));
    } catch (Exception e) {
      SecurityContext.clear();
      // TODO: rethrow & write 4xx
    }
  }
}
