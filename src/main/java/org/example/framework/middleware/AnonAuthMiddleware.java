package org.example.framework.middleware;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.SecurityContext;
import org.example.framework.auth.principal.AnonymousPrincipal;
import org.example.framework.http.Request;

import java.net.Socket;

@Slf4j
public class AnonAuthMiddleware implements Middleware {
  @Override
  public void handle(final Socket socket, final Request request) {
    if (SecurityContext.getPrincipal() != null) {
      return;
    }
    SecurityContext.setPrincipal(new AnonymousPrincipal());
  }
}
