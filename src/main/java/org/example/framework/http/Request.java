package org.example.framework.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Request {
    private String method;
    private String path;
    private Matcher pathMatcher;
    private String query;
    private final Map<String, List<String>> queryParams = new LinkedHashMap<>();
    private String httpVersion;
    private final Map<String, String> headers = new LinkedHashMap<>();
    private byte[] body;
    private Principal principal;

    public String getPathGroup(String name) {
        return pathMatcher.group(name);
    }

    public String getPathGroup(int index) {
        return pathMatcher.group(index);
    }

    public Optional<String> getQueryParam(String name) {
        return Optional.ofNullable(queryParams.get(name))
                .map(e -> e.get(0));
    }

    public List<String> getQueryParams(String name) {
        return queryParams.get(name);
    }

    public Map<String, List<String>> getAllQueryParams() {
        return queryParams;
    }
}
