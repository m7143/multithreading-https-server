package org.example.framework.http;

import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.SecurityContext;
import org.example.framework.exception.*;
import org.example.framework.handler.Handler;
import org.example.framework.middleware.Middleware;
import org.example.framework.parser.QueryParser;
import org.example.framework.util.Bytes;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Builder
@RequiredArgsConstructor
public class Server {
    private static final int MAX_REQUEST_LINE_AND_HEADERS_SIZE = 4096;
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLFCRLF = new byte[]{'\r', '\n', '\r', '\n'};
    private static final int MAX_CONTENT_LENGTH = 10 * 1024 * 1024;

    private final AtomicInteger workerCounter = new AtomicInteger();
    private final ExecutorService workers = Executors.newFixedThreadPool(64, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            final Thread worker = new Thread(r);
            worker.setName("worker-" + workerCounter.incrementAndGet());
            return worker;
        }
    });

    @Builder.Default
    private final AtomicBoolean running = new AtomicBoolean(false);

    @Singular
    private final List<Middleware> middlewares;
    @Singular
    private final Map<Pattern, Map<HttpMethods, Handler>> routes;
    @Builder.Default
    private final Handler notFoundHandler = Handler::notFoundHandler;
    @Builder.Default
    private final Handler methodNotAllowed = Handler::methodNotAllowedHandler;
    @Builder.Default
    private final Handler internalServerErrorHandler = Handler::internalServerError;

    private final ExecutorService serverThread = Executors.newSingleThreadExecutor();

    public void start(final int port) {
        running.set(true);
        serverThread.submit(
         () -> {
            try {
                serveHTTPS(port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    public void serveHTTPS(final int port) throws IOException {
        final ServerSocketFactory socketFactory = SSLServerSocketFactory.getDefault();
        try (
                final ServerSocket serverSocket = socketFactory.createServerSocket(port)
        ) {
            final SSLServerSocket sslServerSocket = (SSLServerSocket) serverSocket;
            sslServerSocket.setEnabledProtocols(new String[]{"TLSv1.2"});
            sslServerSocket.setWantClientAuth(true);

            log.info("server listen on {}", port);
            while (running.get()) {
                try {
                    final Socket socket = serverSocket.accept();
                    workers.submit(() -> requestHandle(socket));
                } catch (Exception e) {
                    log.error("some error", e);
                }
            }
        }
    }

    public void serveHTTP(final int port) throws IOException {
        try (
                final ServerSocket serverSocket = new ServerSocket(port)
        ) {
            log.info("server listen on {}", port);
            while (running.get()) {
                try {
                    final Socket socket = serverSocket.accept();
                    workers.submit(() -> requestHandle(socket));
                } catch (Exception e) {
                    log.error("some error", e);
                }
            }
        }
    }

    public void stop(final int n) throws InterruptedException {
        workers.shutdown();
        if (!workers.awaitTermination(n, TimeUnit.SECONDS)) {
            workers.shutdownNow();
            if (!workers.awaitTermination(n, TimeUnit.SECONDS)) {
                log.info("The pool did not terminate");
            }
        }
        running.set(false);
        serverThread.shutdownNow();
        log.info("server stopped");
    }

    private void requestHandle(Socket acceptedSocket) {
        try (
                final Socket socket = acceptedSocket;
                final InputStream in = new BufferedInputStream(socket.getInputStream());
                final OutputStream out = socket.getOutputStream()
        ) {
            log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());
            final Request request = new Request();
            try {
                final byte[] buffer = new byte[MAX_REQUEST_LINE_AND_HEADERS_SIZE];
                if (!in.markSupported()) {
                    throw new MarkNotSupportedException();
                }
                in.mark(MAX_REQUEST_LINE_AND_HEADERS_SIZE);

                final int requestLineEndIndex = Bytes.indexOf(buffer, CRLF);
                if (requestLineEndIndex == -1) {
                    throw new InvalidRequestStructureException("request line end index not found");
                }
                log.debug("request line end index: {}", requestLineEndIndex);
                final String requestLine = new String(buffer, 0, requestLineEndIndex, StandardCharsets.UTF_8);
                log.debug("request line: {}", requestLine);
                final String[] requestLineParts = requestLine.split("\\s+", 3);
                if (requestLineParts.length != 3) {
                    throw new InvalidRequestLineStructureException(requestLine);
                }

                request.setMethod(requestLineParts[0]);
                request.setHttpVersion(requestLineParts[2]);

                final String pathAndQuery = URLDecoder.decode(requestLineParts[1], StandardCharsets.UTF_8.name());

                final String[] pathAndQueryParts = pathAndQuery.split("\\?", 2);

                final String requestPath = pathAndQueryParts[0];
                request.setPath(requestPath);
                if (pathAndQueryParts.length == 2) {
                    request.setQuery(pathAndQueryParts[1]);
                    request.getQueryParams().putAll(QueryParser.parse(request).getAllQueryParams());
                    log.debug("queryParams {}", request.getAllQueryParams());
                }

                final int headersStartIndex = requestLineEndIndex + CRLF.length;
                final int headersEndIndex = Bytes.indexOf(buffer, CRLFCRLF, headersStartIndex);
                if (headersEndIndex == -1) {
                    throw new InvalidRequestStructureException("headers end is not found");
                }

                int lastProcessedIndex = headersStartIndex;
                int contentLength = 0;
                Map<String, String> headers = new LinkedHashMap<>();
                while (lastProcessedIndex < headersEndIndex - CRLF.length) {
                    final int currentHeaderEndIndex = Bytes.indexOf(buffer, CRLF, lastProcessedIndex);
                    final String currentHeaderLine = new String(buffer, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex);
                    lastProcessedIndex = currentHeaderEndIndex + CRLF.length;

                    final String[] headerParts = currentHeaderLine.split(":\\s*", 2);
                    if (headerParts.length != 2) {
                        throw new InvalidHeaderLineStructureException(currentHeaderLine);
                    }
                    headers.put(headerParts[0], headerParts[1]);

                    if (!headerParts[0].equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH.value())) {
                        continue;
                    }
                    contentLength = Integer.parseInt(headerParts[1]);
                }
                request.getHeaders().putAll(headers);

                if (contentLength < 0) {
                    throw new InvalidContentLengthException();
                }

                if (contentLength > MAX_CONTENT_LENGTH) {
                    throw new InvalidContentLengthException("over the limit");
                }

                final int bodyStartIndex = headersEndIndex + CRLFCRLF.length;
                in.reset();
                final long skipped = in.skip(bodyStartIndex);
                if (skipped < bodyStartIndex) {
                    throw new InvalidMarkException();
                }

                final byte[] body = new byte[contentLength];
                final int bodyRead = in.read(body);
                if (bodyRead != contentLength) {
                    throw new InvalidRequestBodyException();
                }

                log.debug("body {}", body);
                request.setBody(body);
                in.reset();

                log.debug("client connected: {}:{}", socket.getInetAddress(), socket.getPort());


                for (final Middleware middleware : middlewares) {
                    middleware.handle(socket, request);
                }

                final Handler handler;

                Map<HttpMethods, Handler> methodToHandlers = null;
                for (final Map.Entry<Pattern, Map<HttpMethods, Handler>> entry : routes.entrySet()) {
                    final Matcher matcher = entry.getKey().matcher(requestPath);
                    if (!matcher.matches()) {
                        continue;
                    }
                    request.setPathMatcher(matcher);
                    methodToHandlers = entry.getValue();
                }

                if (methodToHandlers == null) {
                    throw new MethodNotAllowedException(request.getMethod());
                }

                handler = methodToHandlers.get(HttpMethods.valueOf(request.getMethod()));
                if (handler == null) {
                    throw new ResourceNotFoundException(requestPath);
                }
                handler.handle(request, out);
            } catch (MethodNotAllowedException e) {
                log.error("request method not allowed", e);
                methodNotAllowed.handle(request, out);
            } catch (ResourceNotFoundException e) {
                log.error("can't found request", e);
                notFoundHandler.handle(request, out);
            } catch (Exception e) {
                log.error("can't handle request", e);
                internalServerErrorHandler.handle(request, out);
            }
        } catch (Exception e) {
            log.error("can't handle request", e);
        } finally {
            SecurityContext.clear();
        }
    }
}
