package org.example.framework.exception;

public class MarkNotSupportedException extends RuntimeException {
  public MarkNotSupportedException() {
  }

  public MarkNotSupportedException(String message) {
    super(message);
  }

}
