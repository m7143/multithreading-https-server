package org.example.framework.exception;

public class InvalidRequestLineStructureException extends InvalidRequestStructureException {
  public InvalidRequestLineStructureException() {
  }

  public InvalidRequestLineStructureException(String message) {
    super(message);
  }
}
