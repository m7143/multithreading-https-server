package org.example.framework.exception;

public class UnsupportAuthenticationToken extends RuntimeException{
    public UnsupportAuthenticationToken() {
    }

    public UnsupportAuthenticationToken(String message) {
        super(message);
    }

    public UnsupportAuthenticationToken(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportAuthenticationToken(Throwable cause) {
        super(cause);
    }

    public UnsupportAuthenticationToken(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
