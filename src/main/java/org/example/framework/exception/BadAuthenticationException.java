package org.example.framework.exception;

public class BadAuthenticationException extends RuntimeException {
  public BadAuthenticationException() {
  }

  public BadAuthenticationException(String message) {
    super(message);
  }
}
