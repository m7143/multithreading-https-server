package org.example.framework.exception;

public class InvalidContentLengthException extends RuntimeException {

    public InvalidContentLengthException() {
        super();
    }

    public InvalidContentLengthException(String message) {
        super(message);
    }
}
