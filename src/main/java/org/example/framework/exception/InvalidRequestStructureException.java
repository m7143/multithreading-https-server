package org.example.framework.exception;

public class InvalidRequestStructureException extends RuntimeException {
  public InvalidRequestStructureException() {
  }

  public InvalidRequestStructureException(String message) {
    super(message);
  }

}
