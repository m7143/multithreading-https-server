package org.example.framework.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;

import java.util.*;

@Slf4j
public class QueryParser {
    private QueryParser() {
    }

    public static Request parse(final Request request) {
        final Map<String, List<String>> queryParams = new LinkedHashMap<>();
        final String query = request.getQuery();
        final String[] pairs = query.split("&");
        for (String pair : pairs) {
            int equalIndex = pair.indexOf("=");
            String key = pair.substring(0, equalIndex);
            String value = pair.substring(equalIndex + 1);
            if (queryParams.containsKey(key)) {
                List<String> values = new ArrayList<>(queryParams.get(key));
                log.debug("key {}, vals {}", key, values);
                values.add(value);
                queryParams.replace(key, values);
                log.debug(" new key {}, vals {}", key, values);
                continue;
            }
            queryParams.put(key, Collections.singletonList(value));
        }
        Request requestOut = new Request();
        requestOut.setPath(request.getPath());
        requestOut.setQuery(request.getQuery());
        requestOut.setMethod(request.getMethod());
        requestOut.setPrincipal(request.getPrincipal());
        requestOut.setHttpVersion(request.getHttpVersion());
        requestOut.setPathMatcher(request.getPathMatcher());
        requestOut.setBody(request.getBody());
        requestOut.getQueryParams().putAll(queryParams);
        requestOut.getHeaders().putAll(request.getHeaders());
        return requestOut;
    }
}
